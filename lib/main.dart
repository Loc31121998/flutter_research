
import 'package:flutter/material.dart';
import 'package:flutter_research/common/share_pref_common.dart';
import 'package:flutter_research/screen/splash/splash_page.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLogging();
  SharedPreferences preferences = await SharedPreferences.getInstance();
  await PrefCommon.init(preferences);
  runApp(MyApp());
}

void setupLogging() {
  Logger.root.level = Level.ALL; // defaults to Level.INFO
  Logger.root.onRecord.listen((record) {
    print('${record.message}');
  });
}
//A//B
class MyApp extends StatelessWidget {
  // This widget is the root of your application.B //C/D
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        backgroundColor: Colors.white,
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashPage(),
    );
  }
}
